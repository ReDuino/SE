// https://gitlab.com/ReDuino/SE/-/tree/2.0.0

#define VERSION_SE 2.0.0
#define FECHA_VERSION_SE 20220710

// tiempo
#define esperar(ESPERA_EN_MS) delay(ESPERA_EN_MS)

#define PEGAR_PARTES(x, y) x##y
#define CONCATENAR(x,y) PEGAR_PARTES(x,y)
#define NOMBRE_VARIABLE_UNICA CONCATENAR(variable_unica_, __LINE__)

static uint32_t *tmp_hacer_cada; // Supongo que el compilador la optimiza
#define hacerDesdeCada(ESPERA_DESDE, REPITE_CADA)                                         \
    static uint32_t NOMBRE_VARIABLE_UNICA = 0; tmp_hacer_cada = &NOMBRE_VARIABLE_UNICA;     \
    if ( (millis() - *tmp_hacer_cada) >= REPITE_CADA*1000 + ESPERA_DESDE*1000)              \
      if ( (*tmp_hacer_cada = *tmp_hacer_cada + REPITE_CADA*1000) == *tmp_hacer_cada)       \

// mensajes
#define pedirHablar() Serial.begin(9600);
#define encenderMensajeria() Serial.begin(9600);

#define enviarMensaje(MSG)  Serial.println(MSG);

// Estados
#define postfx_st_str _postfx_st_str
#define declaroEstados(nombre, val1,  ...)                                       \
       enum CONCATENAR(nombre , _t) {val1 = 0, __VA_ARGS__};                              \
       enum CONCATENAR(nombre , _t) nombre = val1;                                        \
       const char *CONCATENAR(nombre , postfx_st_str)[] = { #val1,  #__VA_ARGS__};        \

#define enviarEstado(estado)  Serial.println(CONCATENAR(estado , postfx_st_str)[estado]);

// Variables
#define declaroTexto(nombre, TEXTO) static char nombre[] = TEXTO;
#define declaroNumero(nombre, VALOR_INICIAL) static int32_t nombre = VALOR_INICIAL;

#define declaroDecimal(nombre, VALOR_INICIAL) static int32_t nombre = VALOR_INICIAL;
#define declaroEntero(nombre, VALOR_INICIAL) static int32_t nombre = VALOR_INICIAL;

#define declararTexto declaroTexto
#define declararNumero declaroNumero
#define declararEstados declararEstados
#define declararDecimal declaroDecimal
#define declararEntero declaroEntero

// Definir tipo patas
#define ENCENDER HIGH
#define APAGAR LOW
#define ENCENDIDO HIGH
#define APAGADO LOW

#define ACTUADOR OUTPUT
#define SENSOR INPUT

#define pataTipo(pata, tipo) pinMode(pata, tipo);
#define escribirPata(pata, valor) digitalWrite(pata, valor);
#define leerPata(pata) digitalRead(pata);

#define pataDigitalTipo(pata, tipo) pataTipo(pata, tipo) 
#define escribirPataDigital(pata, valor) escribirPata(pata, valor)
#define leerPataDigital(pata) leerPata(pata)

// actuar patas
#define ACTUA OUTPUT

#define escribirEstadoPata(pata, estado) pinMode(pata, OUTPUT), digitalWrite(pata, estado );
#define encenderPata(pata) pinMode(pata, OUTPUT), digitalWrite(pata, HIGH );
#define apagarPata(pata) pinMode(pata, OUTPUT), digitalWrite(pata, LOW );
#define alternarPata(pata) pinMode(pata, OUTPUT), digitalWrite(pata, !digitalRead(pata) );

//sensar pata
#define leerEstadoPata(pata, estado) pinMode(pata, INPUT), estado digitalRead(pata);


// condicional
// True or folse ya estan definidos
#define VERDADERO true
#define FALSO false

#define declaroBoleano(nombre, VALOR_INICIAL) static bool nombre = VALOR_INICIAL;

#define alternarBoleano(nombre) nombre = !nombre; // mejor que nombre ^= true???

#define si if
#define sI if
#define Si if
#define SI if
#define siNo else
#define SiNo else
#define sINo else
#define SINo else
#define siNo else
#define SiNO else
#define sIno else
#define SInO else

#define ES_IGUAL_A ==
#define ES_DISTINTO_A !=
#define ES_MAYOR_A >
#define ES_MENOR_A <
#define ES_MAYOR_IGUAL_A >=
#define ES_MENOR_IGUAL_A <=

#define Y &&
#define y &&

#define O ||
#define o ||






































































/* OBSOLETAS

// time
static uint32_t tiempo_ant_arr[8];
#define hacer_cada_num(TIEMPO, num)                   \
    if ( (millis() - tiempo_ant_arr[num]) >= TIEMPO*1000)    \      
      if ( (tiempo_ant_arr[num] = (tiempo_ant_arr[num] + TIEMPO*1000)) == tiempo_ant_arr[num])  \

#define hacer_cada_u(TIEMPO) hacer_cada_num(TIEMPO, __COUNTER__)   


// var unica
// https://stackoverflow.com/questions/35087781/using-line-in-a-macro-definition
#define TOKEN_PASTE(x, y) x##y
#define CAT(x,y) TOKEN_PASTE(x,y)
#define UNIQUE_INT \
  int CAT(prefix, __LINE__)

// https://gitlab.com/ReDuino/SE
// https://refi64.com/posts/overloading-functions-with-the-c-preprocessor.html
// https://github.com/arduino/ArduinoCore-avr/blob/master/cores/arduino/Print.h
// http://pseint.sourceforge.net/i

*/
